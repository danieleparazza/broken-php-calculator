<?php
class MyCalculator {
    private $_fval, $_sval;
    public function __construct( $fval, $sval ) {
        $this->_fval = $fval;
        $this->_sval = $sval;
    }
    public function add() {
        $result = $this->_fval + $this->_sval;
        $result > 10 ? $result++ : $result;
        return $result;
    }
    public function subtract() {
        return $this->_fval - $this->_sval;
    }
    public function multiply() {
        return $this->_fval * $this->_sval;
    }
    public function divide() {
        return $this->_fval / $this->_sval;
    }
}
?>