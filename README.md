# README #


### What is this repository for? ###

This is an educational repository used as an early example of usage of the git workflow. You have a master branch which contains two bugs to fix. It's also present a develop branch with the bugfixes implemented and committed.

The application is PHP calculator that has some bugs to fix. First you have to deal with a 500 - Internal Server Error and then with an awkward bug in the "add" method

### How do I get set up? ###

Clone the repository and visit index.php